package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	cpuUsage = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "billing_cpu_utilization",
		Help: "Value Per CPU Utilization",
	}, []string{"namespace"})
)

var (
	devTeam = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "billing_cost_devteam",
		Help: "Cost of DevTeam",
	}, []string{"namespace"})
)

func init() {
	// Metrics have to be registered to be exposed:
	prometheus.MustRegister(cpuUsage)
	prometheus.MustRegister(devTeam)
}

type costsData struct {
	label     string
	number    float64
	frequence float64
	cost      float64
}

var myClient = &http.Client{Timeout: 10 * time.Second}

func getJSON(url string, target interface{}) error {
	r, err := myClient.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func getPromResult(url string) float64 {

	results := new(resultPrometheus)
	getJSON(url, results)
	resultado := results.Data.Result[0].Value[1].(string)

	i, _ := strconv.ParseFloat(resultado, 64)

	return i

}

type resultPrometheus struct {
	Status string `json:"status"`
	Data   struct {
		ResultType string `json:"resultType"`
		Result     []struct {
			Metric struct {
				Name     string `json:"__name__"`
				Instance string `json:"instance"`
				Job      string `json:"job"`
			} `json:"metric"`
			Value []interface{} `json:"value"`
		} `json:"result"`
	} `json:"data"`
}

//Create a list of structs
var listNameSpaces [2]costsData
var listTeams [2]costsData

//values
var value_cpu_month, cpu_per_container, finalCost float64

/*

Function to calculate the Final Cost

*/
func calculeFinalCost(listNameSpaces costsData) float64 {

	finalCost := listNameSpaces.number * listNameSpaces.frequence * listNameSpaces.cost

	return finalCost
}

/*
Function that initialize All the Main Variables
*/
func initializeValues() {

	cpu_per_container = 2.0
	value_cpu_month = 59.904

	//Query in Prometheus
	query_sum1 := ""
	resultQuery1 := getPromResult("" + query_sum1) //url to query

	query_sum2 := ""
	resultQuery2 := getPromResult("" + query_sum2) //url to query

	//Initialize with values Hobb
	listNameSpaces[0].label = "" //name of account
	listNameSpaces[0].number = resultQuery1
	listNameSpaces[0].frequence = 1.0
	listNameSpaces[0].cost = cpu_per_container * value_cpu_month

	//Initialize with values Garoupinha
	listNameSpaces[1].label = "" //name of account
	listNameSpaces[1].number = resultQuery2
	listNameSpaces[1].frequence = 1.0
	listNameSpaces[1].cost = cpu_per_container * value_cpu_month

	//Team Garoupinha
	listTeams[0].label = "" //name of account
	listTeams[0].number = 6.0
	listTeams[0].cost = 250.0
	listTeams[0].frequence = 120.0

	//Tema Hobb
	listTeams[1].label = "" //name of account
	listTeams[1].number = 5.0
	listTeams[1].cost = 250.0
	listTeams[1].frequence = 120.0

	for i := range listNameSpaces {
		cpuUsage.WithLabelValues(listNameSpaces[i].label).Set(calculeFinalCost(listNameSpaces[i]))

	}

	for i := range listTeams {
		devTeam.WithLabelValues(listTeams[i].label).Set(calculeFinalCost(listTeams[i]))

	}

}

func main() {

	go initializeValues()

	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":7777", nil))

}
