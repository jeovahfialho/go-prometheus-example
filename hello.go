/*Inform how is the main package*/
package main

/* Package to help with formatting */
import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

/* COnexao com o prometheus */

/* Global Var to use fmt.Println */
var p = fmt.Println

/* Main function of our program and first that is going to be executed */
// func main() {

// 	p("Starting GO Application of Billing...")

// 	getDataFromURL(getURL())
// }

func getURL() string {

	url := "" //Prometheus URL metrics

	return url
}

func connectPrometheus() {

	fmt.Println("hello")
	fmt.Println("jeova")

}

func getDataFromURL(url string) {

	countLine := 0

	type linesPrometheus struct {
		metricsName string
	}

	lines := []*linesPrometheus{}

	response, err := http.Get(url)

	if err != nil {

		fmt.Printf("%s", err)
		os.Exit(1)

	} else {

		defer response.Body.Close()

		contents, err := ioutil.ReadAll(response.Body)

		if err != nil {
			fmt.Printf("%s", err)
			os.Exit(1)
		}

		s := make([]string, 200)

		countChars := 0

		for _, v := range contents {

			if string(v) == "\n" {

				/*

					Don't save any line that begin with a "#"

				*/
				if s[0] != "#" {

					/*

						Create an instance of the struct linePrometheus.
						Set the string value into the correct struct's field.
						Append the line into a List.

					*/
					loc := new(linesPrometheus)
					loc.metricsName = strings.Join(s, "")
					lines = append(lines, loc)

				}

				countLine++
				countChars++

				/* Clear the slice s. Is the same set the command nil. But we also need to realoce again. */
				s = make([]string, 200)

			} else {

				/* Each char of contents becames a byte into a slice */
				s[countChars] = string(v)
				countChars++

			}

		}

		/*

			Just for test. List all the instance into Append.

		*/
		for i := range lines {

			line := lines[i]
			fmt.Println(line.metricsName)

		}

	}

}
